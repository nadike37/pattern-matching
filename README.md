#  Pattern Matching in Java

![Pattern Matching Fundamentals](/src/main/resources/pattern_matching_short_explanation.png)

***

## Table of Contents
- [Description](#description)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [How To Go Through The Project](#how-to-go-through-the-project)
- [Roadmap](#roadmap)
- [Authors](#authors)
- [License](#license)
- [Project status](#project-status)
- [References](#references)

***

## Description
###### The aim of this project is to explain and provide use cases (how tos) regarding Pattern Matching using Java programming language.<br/>The Pattern Matching was developed and 'upgraded' in time starting with Java version 14.</br>Therefore a chronological approach is used to understand the concept, step by step, as it was developed and integrated by Java engineers.

## Prerequisites
- [ ] [Java 21](https://www.oracle.com/ro/java/technologies/downloads/)
- [ ] [Apache maven](https://maven.apache.org/download.cgi)
- [ ] [Git SCM](https://www.git-scm.com/download/win)
- [ ] [IntelliJ IDEA](https://www.jetbrains.com/idea/download/?section=windows)

> **_NOTE:_** The projects runs with **PREVIEW FEATURE** enabled. The following compiler argument was added in Maven (pom.xml)
```
  <compilerArgs>
    <arg>--enable-preview</arg>
  </compilerArgs>
```

## Installation
- [ ] Download and install Java 21
- [ ] Download and install Apache Maven
- [ ] Download and install Git
- [ ] Download and install IntelliJ IDEA
- [ ] Open Intellij IDEA
- [ ] Inside IntelliJ:
  - [ ] Ctrl + Shift + Alt + S to open Project Structure
  - [ ] Add the SDK version
  - [ ] Open the project: File -> Open -> Browse and select the folder where the project was cloned
  - [ ] Load Maven for the project

## How To Go Through The Project


## Roadmap
This project will be updated with a 6 moths cadence due to the Oracle fixed time-based releases of Java versions.

## Authors
nadike37

## License
MIT licensed.

## Project status
New use cases will be defined once Java enriches this "feature".

## References:
https://www.youtube.com/watch?v=-iWQXvKNbFE
https://www.youtube.com/watch?v=V_mzFdFTk6I
https://www.youtube.com/watch?v=aKaw9W789wU

> **_NOTE:_**  README file under construction.
