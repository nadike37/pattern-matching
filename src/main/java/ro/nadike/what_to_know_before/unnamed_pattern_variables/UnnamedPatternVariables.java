package ro.nadike.what_to_know_before.unnamed_pattern_variables;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static java.util.function.UnaryOperator.identity;
import static java.util.stream.Collectors.toMap;

/**
 * A preview feature of JDK 21
 * <p>
 * Focusing in reducing boilerplate code when side effects are the only concern.
 * <p>
 * They can be defined as many times as needed, but they cannot be referenced from a later point.
 */
public class UnnamedPatternVariables {

    public static void main(String[] args) {
        lambdaParameters(List.of("aaa", "vvv"));
    }

    public void forLoop(List<String> colors, int limit) {
        int total = 0;
        // the unnamed variable is represented by an underscore
        for (var _ : colors) {
            total++;
            if (total > limit) {
                // side effect
            }
        }
    }

    public void tryCatch(String fileName) {

        try {
            FileReader reader = new FileReader(fileName);
        } catch (FileNotFoundException _) {
            System.out.println("The file was not found");
        }
    }

    public static void lambdaParameters(final List<String> names) {
        names.stream()
                .distinct()
                .collect(toMap(identity(),
                        _ -> new ArrayList<String>()))
                .values()
                .forEach(System.out::println);

    }
}