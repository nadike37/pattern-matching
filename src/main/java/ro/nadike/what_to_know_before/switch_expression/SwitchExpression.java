package ro.nadike.what_to_know_before.switch_expression;

/**
 * In Java 12, enhanced switch expressions were introduced as a preview feature,
 * eventually becoming a standard feature in Java 16.
 * These improved switch expressions offer a multitude of advantages compared to the traditional switch statements.
 * They offer a number of improvements over traditional switch statements, including:
 * <p>
 *     Switch STATEMENTS vs switch EXPRESSIONS
 * The main difference is that
 *      - while a switch statement can be used to update the value of a predefined variable;
 *      - a switch expression is assigned to a variable. This is possible because a switch expression evaluates to a specific value.
 * Additionally:
 *      - switch expressions introduced a new arrow syntax that condenses the code and makes it more readable
 *      - and eliminates the need for break statements.
 */

public class SwitchExpression {

    public static void main(String[] args) {
        switchExpression(Weekdays.SATURDAY);
        switchStatement(Weekdays.SATURDAY);
    }

    private static int switchExpression(Weekdays day) {
        return switch (day) {
            // Arrow syntax: The new arrow syntax (case MONDAY ->) is more concise and easier to read than the traditional colon syntax (case MONDAY:).
            case MONDAY -> 4;
            // Multiple values per case: We can now specify multiple values per case, using commas to separate them.
            // This eliminates the need to use fall-through cases.
            case TUESDAY, WEDNESDAY, THURSDAY -> 8;
            // Yield statement: We can use the yield keyword to return a value from a switch expression.
            // This makes it easier to use switch expressions in expressions and assignments.
            case FRIDAY -> {
                yield 6;
            }
            case SATURDAY, SUNDAY -> 0;
            // since a switch expression must be complete or exhaustive the default type is not needed
            // the compiler will enforce you to declare all the possible cases
            default -> throw new IllegalArgumentException("What a day?!" + day.name());
            //notice the semicolon
        };
    }

    private static int switchStatement(Weekdays weekday) {
        int numberOfWorkingHours;

        switch (weekday) {
            case MONDAY:
                numberOfWorkingHours = 4;
                break;
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
                numberOfWorkingHours = 8; // 1.5% for Checking Account
                break;
            case FRIDAY:
                numberOfWorkingHours = 6; // 3% for Business Account
                break;
            case SUNDAY:
            case SATURDAY:
                numberOfWorkingHours = 0;
                break;
            default:
                throw new IllegalArgumentException("What a day?!" + weekday.name());
        }
        return numberOfWorkingHours;
    }
}
