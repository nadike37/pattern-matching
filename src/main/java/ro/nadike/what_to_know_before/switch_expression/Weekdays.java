package ro.nadike.what_to_know_before.switch_expression;

public enum Weekdays {

    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY;
}
