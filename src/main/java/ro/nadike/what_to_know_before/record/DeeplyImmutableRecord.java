package ro.nadike.what_to_know_before.record;

import java.util.List;

/**
 * If a record contains a possible mutable object as a field, this will break the immutability of the java record class.
 * For example, if your record contains a list field,
 * the canonical constructor will just assign the given list to the field and in the case of a change of list content, the record class field will also be changed.
 * The following code example has been provided to demonstrate how to achieve truly immutability.
 */
public record DeeplyImmutableRecord(String name, List<Integer> marks) {

    /**
     * defensive copy of a collection
     */
    public DeeplyImmutableRecord {
        marks = List.copyOf(marks);
    }
}
