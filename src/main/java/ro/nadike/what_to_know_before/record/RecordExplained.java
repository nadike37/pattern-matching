package ro.nadike.what_to_know_before.record;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Contextual keyword "record" is used to declare a record.
 * <p>
 * A record can be generic.
 * <p>
 * Instance variables are declare inside parentheses between the name of the record and the body of the record
 * <p>
 * !!! NO other instance fields can be declared inside the body of the record
 * <p>
 * A record CANNOT EXTEND any other class, but CAN IMPLEMENT as many interfaces are needed
 * <p>
 * Furthermore, a record cannot be extended (being a final class)
 */
public record RecordExplained<T>(String name,
                                 List<ImmutableJavaBean> immutableBeans) /*implements Runnable, ...*/ {

    /**
     * static instance variable/s can exist/s inside a record
     */
    public static String staticField;

    /**
     * constant/s can exist/s inside a record
     */
    public static final int CONSTANT = 1;

    /**
     * A record can contain instance or static methods
     */
    public String instanceMethod() {
        return "I am a record";
    }

    /**
     * The CANONICAL CONSTRUCTOR is provided by default by the JVM
     * You do not need to write it explicitly
     * <p>
     *    public RecordExplained(String name, List<ImmutableJavaBean> immutableBeans) {
     *        this.name = name;
     *        this.immutableBeans = immutableBeans;
     *    }
     */

    /**
     * COMPACT CONSTRUCTOR
     * A shorthand for the canonical constructor
     * You do not need to explicitly declare the arguments of the constructor
     * The compiler knows, contextually, what are the arguments
     */
    public RecordExplained {
        if (Objects.isNull(name)) {
            throw new IllegalArgumentException("The name must have a value!");
        }
        immutableBeans = List.copyOf(immutableBeans);
    }

    /**
     * If other/s constructor/s (NON-CANONICAL) are declared they must call the canonical constructor
     * <p>
     * This is NOT a valid custom constructor for a record
     * <p>
     * COMPILER ERROR
     * public RecordExplained(String name) {
     *         this.name = name;
     *         this.immutableBeans = Collections.emptyList();
     * }
     */
    public RecordExplained(String name) {
       this(name, Collections.emptyList());
    }
}