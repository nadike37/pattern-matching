package ro.nadike.what_to_know_before.record;

/**
 * Records are immutable data classes that require only the type and name of fields.
 * <p>
 * Java compiler generates:
 *      - private final fields;
 *      - public getters methods, whose names match the name of our field
 *          In this case: city(), street(), streetNumber()
 *      - public constructor with an argument for each field;
 *          Constructor can be customizable
 *          This customization is intended to be used for validation and should be kept as simple as possible.
 *      - methods: equals(Object o), hashCode(), toString();
 * <p>
 * Main advantages:
 *      - Reducing boilerplate code for data transfer objects
 *      - Making immutable object creation easy and handy
 * <p>
 * Records are SHALLOWLY IMMUTABLE data carriers primarily consisting of their state’s declaration.
 */
public record Record(String name,
                     String surname) {
}
