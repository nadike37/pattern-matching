package ro.nadike.what_to_know_before.record;

import java.util.List;

public final class ImmutableJavaBean {

    private final String town;
    private final String county;
    private final List<Integer> streets;

    public ImmutableJavaBean(final String town,
                             final String county,
                             final List<Integer> streets) {
        this.town = town;
        this.county = county;
        this.streets = List.copyOf(streets);
    }

    public String getTown() {
        return town;
    }

    public String getCounty() {
        return county;
    }

    public List<Integer> getStreets() {
        return streets;
    }
}
