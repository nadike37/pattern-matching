package ro.nadike.what_to_know_before.abstraction.traditional_way;

public class Rectangle implements Shape {

    private final double width;
    private final double length;

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double area() {
        return width * length;
    }
}