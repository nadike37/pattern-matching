package ro.nadike.what_to_know_before.abstraction.traditional_way;

/**
 * If, supposedly, you have to calculate the area for many shapes (circle, rectangle, triangle)
 * You will be tempted to have an interface 'Shape' with an abstract method called 'area()'
 * And each shape, Circle, Rectangle and Triangle to implement this Shape interface
 * And each concrete class will provide implementation for the abstract method 'area()'
 * At the first glance this looks very safe since the compiler will force you to implement the 'area()' method
 * But there is a price to be paid for this to maintain the code with the new incoming features you have to develop for that certain model
 *
 * !!! check PATTERN MATCHING for RECORDs with SEALED TYPEs to see the new/better approach
 */
public interface Shape {

    double area();
}
