package ro.nadike.what_to_know_before.sealed_types;


/**
 * A permitted subclass may also be declared sealed.
 * However, if we declare it non-sealed, then it is open for extension.
 */
public non-sealed class Car extends Vehicle {
}
