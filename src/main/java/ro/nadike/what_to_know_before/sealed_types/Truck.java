package ro.nadike.what_to_know_before.sealed_types;

/**
 * A permitted subclass must define a modifier.
 * It may be declared final to prevent any further extensions.
 */
public final class Truck extends Vehicle {
}
