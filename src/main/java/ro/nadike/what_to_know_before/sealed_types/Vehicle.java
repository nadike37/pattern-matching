package ro.nadike.what_to_know_before.sealed_types;

/**
 * To seal an interface or a class, we can apply the sealed modifier to its declaration.
 * The permits clause then specifies the classes that are permitted to implement/extend the sealed interface/class.
 * <p>
 * All permitted subclasses must belong to the same module as the sealed class or if a module was not declare subclasses must belong to the same package.
 * Every permitted subclass must explicitly extend the sealed class.
 * Every permitted subclass must define a modifier: final, sealed, or non-sealed.
 */
public sealed class Vehicle permits Car, Truck {
}
