package ro.nadike.pattern_matching_explained.instanceof_operator;

/**
 * since JDK 14
 */
public class PatternMatchingForInstanceof {

    public static void main(String[] args) {
        instanceofPatternMatching_1("a string");
        instanceofPatternMatching_2("a string");
        instanceofPatternMatching_flowScoping_1("a string");
        instanceofPatternMatching_flowScoping_1(26);
    }

    /**
     * if the predicate (obj instanceof String) is evaluated to true
     * the pattern variable we are going to use is "s"
     */
    private static void instanceofPatternMatching_1(Object obj) {
        if (obj instanceof String s) {
            System.out.println(s.length());
        } else {
            // Use of "s" variable is not allowed here
            // because here we do not have a String
            // so the scope of "s" is not valid
        }
    }

    private static void instanceofPatternMatching_2(Object obj) {
        if (obj instanceof String s && s.length() > 100) {
            System.out.println("I am a very long string " + s.length());
        }

        // OR logical operator cannot be used in this scenario
        // because the right-hand side will be evaluated only if the left-hand side is evaluated to false
        // COMPILER ERROR
        /*if(obj instanceof String s || s.length() > 100) {
            System.out.println("I am a very long string " + s.length());
        }*/
    }

    /**
     * SCOPE OF THE BINDING VARIABLE/S (FLOW SCOPING)
     * - the scope o f a biding variable is set of places in the program where it would be definitely assigned
     */
    private static void instanceofPatternMatching_flowScoping_1(Object obj) {
        if (!(obj instanceof String s)) {
            return;
        }

        // scope of s is valid
        System.out.println(s.length());

        // other lines of code


        // scope of s is still valid
        System.out.println(s.length());
    }

    /**
     * Need FLOW SCOPING to be able to reuse "num" as the variable name
     */
    private static void instanceofPatternMatching_flowScoping_2(Object obj) {
        if (obj instanceof Integer num) {
            // ...
        } else if (obj instanceof Double num) {
            // ...
        } else if (obj instanceof Long num) {
            // ...
        }
    }
}
