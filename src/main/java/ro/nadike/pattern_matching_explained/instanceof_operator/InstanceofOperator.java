package ro.nadike.pattern_matching_explained.instanceof_operator;

public class InstanceofOperator {

    /**
     * This is being in Java right from the very beginning
     * <p>
     * Allows to examine the type of a reference and see if it is of:
     *      - a specific type
     *      - any of the super types of that type
     *      - any of the interfaces or super interfaces that implements
     */
    private void instanceofOldWay(Object obj) {
        if (obj instanceof String) {
            // we must always perform an explicit cast with an assignment
            String s = (String) obj;
            System.out.println(s.length());
        }
    }
}