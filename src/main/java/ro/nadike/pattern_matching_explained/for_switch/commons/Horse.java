package ro.nadike.pattern_matching_explained.for_switch.commons;

public final class Horse extends Animal {

    private final int age;

    public Horse(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
}
