package ro.nadike.pattern_matching_explained.for_switch;

import ro.nadike.pattern_matching_explained.for_switch.commons.Animal;
import ro.nadike.pattern_matching_explained.for_switch.commons.Horse;
import ro.nadike.pattern_matching_explained.for_switch.commons.Shark;

/**
 * GuardedPattern:
 *      PrimaryPattern && ConditionalAndExpression
 * <p>
 *      Uses sealed types (classes or interfaces)
 */
public class GuardedPatterns {

    public void animalTester(Animal animal) {
        switch (animal) {
            // "when" is a contextual keyword
            case Horse h when h.getAge() < 1 -> System.out.println("The horse must drink milk");
            case Horse h -> System.out.println("Adult horse");
            case Shark s -> System.out.println("I am a shark and I live in ocean");
            case Animal a -> System.out.println("Can be any animal " + a);
        }
    }

    /**
     * Less specific cases must NOT hide more specific cases
     * <p>
     * It's a compile time error for a label in a switch block to be dominated by an earlier label in that switch block
     */
    public void animalTester_patternDominance(Animal animal) {
        switch (animal) {
            // because Animal is a super-class for Horse and Shark
            // the cases underneath are unreachable
            case Animal a -> System.out.println("Can be any animal " + a);
            //  case Horse h -> System.out.println("Adult horse");
            //  case Shark s -> System.out.println("I am a shark and I live in ocean");
        }
    }

    public void animalTester_patternDominance_2(Animal animal) {
        switch (animal) {
            case Horse h -> System.out.println("Adult horse");
            // the guarded pattern will NOT be reached because is "dominated" by the line from above
            // case Horse h when h.getAge() < 1 -> System.out.println("The horse must drink milk");
            case Shark s -> System.out.println("I am a shark and I live in ocean");
            case Animal a -> System.out.println("Can be any animal " + a);
        }
    }

    public void animalTester_patternDominance_3(Animal animal) {
        switch (animal) {
            // the guarded pattern dominates the unguarded
            case Horse h when true -> System.out.println("The horse must drink milk");
            // case Horse h -> System.out.println("Adult horse");
            case Shark s -> System.out.println("I am a shark and I live in ocean");
            case Animal a -> System.out.println("Can be any animal " + a);
        }
    }
}
