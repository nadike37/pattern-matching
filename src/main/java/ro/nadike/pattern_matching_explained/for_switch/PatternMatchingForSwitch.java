package ro.nadike.pattern_matching_explained.for_switch;

import ro.nadike.pattern_matching_explained.for_switch.commons.Commodities;

/**
 * Switch was extended to allow type patterns to be matched
 */
public class PatternMatchingForSwitch {

    private void testObject_nullCaseDeclared(Object obj) {
        switch (obj) {
            // null is a special case
            // can be used in all switch statement and expressions
            // if not included it will be added by the compiler at start (throwing NullPointerException)
            case null -> System.out.println("I am null");
            case String s -> System.out.println(s);
            case Integer i -> System.out.println(i * i);
            // you cannot have a case of a primitive because primitives are not types
            // but an array is an object
            case int[] arr -> System.out.println(arr.length);
            default -> System.out.println("i am not the type you are looking for");
        }
    }

    private void testObject_nullCaseAddedByCompiler(Object obj) {
        switch (obj) {
            // this line will be added by the compiler if no explicit case for null was not declared
            // case null -> throw new NullPointerException();
            case String s -> System.out.println(s);
            case Integer i -> System.out.println(i * i);
            // you cannot have a case of a primitive because primitives are not types
            // but an array is an object
            case int[] arr -> System.out.println(arr.length);
            default -> System.out.println("i am not the type you are looking for");
        }
    }

    private void testObject_nullAndDefaultCombined(Object obj) {
        switch (obj) {
            case String s -> System.out.println(s);
            case Integer i -> System.out.println(i * i);
            // you cannot have a case of a primitive because primitives are not types
            // but an array is an object
            case int[] arr -> System.out.println(arr.length);
            // combining null and default cases
            case null, default -> System.out.println("i am not the type you are looking for");
        }
    }

    /**
     * since a pattern switch statement and all switch expressions must be complete or exhaustive
     * all possible values must be handled
     */
    private void testObject_completenessByDefault(Object obj) {
        switch (obj) {
            case String s -> System.out.println(s);
            case Integer i -> System.out.println(i * i);
            default -> System.out.println("i am not the type you are looking for");
        }
    }

    /**
     * NOT all pattern switch statements or all switch expressions must have a default
     */
    private String commoditiesDescription(Commodities commodity) {
        return switch (commodity) {
            case GAS -> "Gas is bad for the environment";
            case GOLD -> "Metal";
            case OIL -> "Must be replaced by a cleaner alternative";
            // "default" branch is unnecessary since all the possible values are taken into consideration
            // as a best practice the default case should NOT be declared
            // and the focus must be on handling all the possible cases
            default -> "I do not know";
        };
    }
}