package ro.nadike.pattern_matching_explained.for_switch.sealed_types.same_file;

import java.util.List;


/**
 * The sealed feature introduces a couple of new modifiers and clauses in Java: sealed, non-sealed, and permits.
 * If you have a sealed interface and records, declared in the same file, that are implementing that interface you are not obliged to use 'permits' keyword
 */
public sealed interface Person{

    record BabyChild(String name) implements Person {
    };

    record Student(String name, List<Double> marks) implements Person {
    };

    record Employee(String name, double salary) implements Person {
    };
}
