package ro.nadike.pattern_matching_explained.for_switch.sealed_types.same_file;

import ro.nadike.pattern_matching_explained.for_switch.sealed_types.same_file.Person;

public class PersonBehavior {

    private String personSays(final Person person) {
        return switch (person) {
            case Person.Employee(String name, double _) -> "May name is " + name + " and my salary is confidential";
            case Person.Student(String name, var marks) -> "May name is " + name + " and my marks average is " + marks.stream().mapToDouble( i -> i).average();
            case Person.BabyChild _ -> "I am playing all day long";
        };
    }
}
