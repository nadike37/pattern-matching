package ro.nadike.pattern_matching_explained.record;

import ro.nadike.pattern_matching_explained.record.commons.Point;

/**
 * Record pattern are a de-construction pattern
 * De-construction pattern only works with records
 */
public class PatternMatchingRecord {

    public void sum(Object o) {
        if (o instanceof Point p) {
            System.out.println("The sum is: " + p.first() + p.second());
        }
    }

    /**
     * in the case the predicate is matched the record declaration is provided
     * the record is de-constructed, and then we can reference the values contained in the record
     */
    public void sum_recordDeconstructed(Object o) {
        if (o instanceof Point(double f, double s)) {
            System.out.println("The sum is: " + f + s);
        }
    }
}