package ro.nadike.pattern_matching_explained.record.commons;

public record ColorPoint(Point point,
                         Color color) {
}
