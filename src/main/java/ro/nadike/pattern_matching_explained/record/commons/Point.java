package ro.nadike.pattern_matching_explained.record.commons;

public record Point(double first, double second) {
}
