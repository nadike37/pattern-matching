package ro.nadike.pattern_matching_explained.record.commons;

public record ColorSquare(ColorPoint topLeft,
                          ColorPoint bottomRight) implements Square {
}
