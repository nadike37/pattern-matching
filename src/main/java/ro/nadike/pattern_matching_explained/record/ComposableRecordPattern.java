package ro.nadike.pattern_matching_explained.record;

import ro.nadike.pattern_matching_explained.record.commons.*;

/**
 * De-constructing nested records
 */
public class ComposableRecordPattern {

    public void printColor_1(Square square) {
        if (square instanceof ColorSquare(ColorPoint topLeft, ColorPoint bottomRight)) {
            System.out.println(topLeft.color());
        }
    }

    public void printColor_2(Square square) {
        if (square instanceof ColorSquare(ColorPoint(Point p, Color c), ColorPoint bottomRight)) {
            System.out.println(c);
        }
    }

    public void printTopLeftF(Square square) {
        if (square instanceof ColorSquare(ColorPoint(Point(double f, double s), Color c), ColorPoint bottomRight)) {
            System.out.println(f);
        }
    }

    /**
     * Patterns and Local Variable Type Inference
     */
    public void printTopLeftF_2(Square square) {
        if (square instanceof ColorSquare(ColorPoint(Point(var f, var s), var c), var bottomRight)) {
            System.out.println(f);
        }
    }

    /**
     * Any Pattern / Unnamed patterns are an improvement over Record Patterns
     * <p>
     * Unnamed Variables are described by underscore '_'
     * <p>
     * They can be defined as many times as needed, but they cannot be referenced from a later point.
     * <p>
     * is a PREVIEW feature inside JDK 21
     */
    public void printTopLeftF_UnnamedVariables(Square square) {
        if (square instanceof ColorSquare(ColorPoint(Point(var f, _), _), _)) {
            System.out.println(f);
        }
    }
}
